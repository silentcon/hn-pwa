import firebase from 'firebase';


export function getItemRef(id) {
  const url = '/v0/item';
  return firebase.database().ref(`${url}/${id}`)
}
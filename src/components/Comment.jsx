import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { getItemRef } from '../util';
import './Comment.css';

class Comment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      item: {}
    }
  }

  componentDidMount() {
    // Fetch this comment and its child comments
    const { id } = this.props;
    getItemRef(id)
      .once('value')
      .then(snapshot => {
        if (snapshot && snapshot.val()) {
          this.setState({
            item: snapshot.val()
          })
        }
      });
  }

  renderChildComments() {
    const { item } = this.state;
    const { id, level } = this.props;

    if (item && item.kids) {
      return item.kids.map(id =>
        <Comment key={id} id={id} level={level + 1} />
      );
    } else {
      return null;
    }
  }

  render() {
    const { item } = this.state;

    if (item) {
      return (
        <div className="Comment">
          <div className="Comment--body">
            <div className="Comment--header">
              {item.by}{'     '}
              {moment.unix(item.time).fromNow()}
            </div>
            <div dangerouslySetInnerHTML={{__html: item.text}} />
          </div>
          {this.renderChildComments()}
        </div>
      )
    } else {
      return null;
    }
  }
}

Comment.propTypes = {
  id: PropTypes.number.isRequired,
  level: PropTypes.number
}

Comment.defaultProps = {
  level: 0
}

export default Comment;
import firebase from 'firebase'
import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import StoriesView from './views/StoriesView';
import ItemView from './views/ItemView';

import logo from './logo.svg';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props)

    // Setup firebase
    firebase.initializeApp({
      databaseURL: "https://hacker-news.firebaseio.com"
    })
  }

  render() {
    return (
      <Router>
        <div className="theme-dark">
          <Route exact path="/" component={StoriesView} />
          <Route path="/item/:id" component={ItemView} />
        </div>
      </Router>
    );
  }
}

export default App;

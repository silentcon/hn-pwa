import firebase from 'firebase';
import React from 'react';
import './ItemView.css';
import { getItemRef } from '../util';

import Comment from '../components/Comment';
import CommentList from '../lists/CommentsList';

const status = {
  LOADING: 0,
  SUCCESS: 1,
  ERROR: 2
}

class ItemView extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      item: {},
      status: status.LOADING
    }
  }

  componentDidMount() {
    // Fetch story from firebase
    const itemId = this.state.id;
    const cached = window.sessionStorage.getItem(itemId);
    if (cached) {
      this.setState({
        item: JSON.parse(cached)
      })
    } else {
      getItemRef(itemId)
        .once('value')
        .then(snapshot => {
          if (snapshot && snapshot.val()) {
            const item = snapshot.val();
            this.setState({
              item,
              status: status.SUCCESS
            });
          } else {
            this.setState({
              status: status.ERROR
            })
          }
        });
    }
  }

  componentWillUnmount() {
    console.log(window.location)
  }

  renderStory() {
    const { item } = this.state

    if (item) {
      return (
        <div className="ItemView--story">
          <h1 className="title">{item.title}</h1>
          <h2 className="subtitle is-6">
            <a href={item.url}>{item.url}</a>
          </h2>
        </div>
      );
    }
  }

  renderComments() {
    const { item } = this.state;

    if (item) {
      return <CommentList ids={item.kids} />
    } else {
      return null;
    }
  }

  render() {
    return (
      <div className="ItemView container">
        {this.renderStory()}
        {this.renderComments()}
      </div>
    )
  }
}

export default ItemView;
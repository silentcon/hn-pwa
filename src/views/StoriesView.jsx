import React from 'react';
import firebase from 'firebase';
import './StoriesView.css';

import StoriesList from '../lists/StoriesList';

class StoriesView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			ids: []
		}
	}

	componentDidMount() {
		// Fetch topstories ids
		const cached = window.sessionStorage.getItem('stories');
		if (cached) {
			console.log('loading from cache')
			this.setState({
				ids: JSON.parse(cached)
			});
		} else {
			this.getTopStories()
				.then(snapshot => {
					if (snapshot && snapshot.val()) {
						this.setState({
							ids: snapshot.val()
						});
						window.sessionStorage.setItem('stories', JSON.stringify(snapshot.val()));
					}
				});
		}

		const prevScroll = window.sessionStorage.getItem('scrollPos');
		if (prevScroll) {
			window.scrollTo(0, prevScroll);			
		}
	}

	componentWillUnmount() {
		window.sessionStorage.setItem('scrollPos', window.scrollY)
	}

	getTopStories() {
		return firebase.database()
			.ref('/v0/topstories')
			.once('value')
	}

	render() {
		return (
			<div className="StoriesView container">
				<StoriesList ids={this.state.ids} />
			</div>
		);
	}
}

export default StoriesView;
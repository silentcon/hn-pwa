import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './StoriesList.css';

import StoryRow from './StoryRow';

class StoriesList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: []
    }
  }

  renderStory(id, index) {
    return (
      <li>
        <StoryRow key={id} id={id} index={index}/>
      </li>
    )
  }

  render() {
    const { ids } = this.props;

    if (ids) {
      console.log(ids)

      return (
        <ul>
          {ids.map((id, index) => this.renderStory(id, index))}
        </ul>
      );
    } else {
      return null;
    }
  }
}

StoriesList.propTypes = {
  ids: PropTypes.arrayOf(PropTypes.number).isRequired
}

export default StoriesList;
import React from 'react';
import PropTypes from 'prop-types';
import './CommentsList.css';

import Comment from '../components/Comment';

class CommentList extends React.Component {  
  render() {
    const { ids } = this.props;

    if (ids) {
      return ids.map(id => 
        <Comment key={id} id={id}/>
      );
    } else {
      return null;
    }
  }
}

CommentList.propTypes = {
  ids: PropTypes.arrayOf(PropTypes.number).isRequired
}

export default CommentList;
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './StoryRow.css';
import { getItemRef } from '../util';
import { Link } from 'react-router-dom';

class StoryRow extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      item: {}
    };
  }

  componentDidMount() {
    // Fetch item from id from props
    const { id } = this.props;
    if (id) {
      const cached = window.sessionStorage.getItem(id)
      if (cached) {
        this.setState({
          item: JSON.parse(cached)
        })
      } else {
        getItemRef(id)
          .once('value')
          .then(snapshot => {
            if (snapshot && snapshot.val()) {
              this.setState({
                item: snapshot.val()
              });
              window.sessionStorage.setItem(id, JSON.stringify(snapshot.val()));
            }
          });
      }
    }
  }

  render() {
    const { item } = this.state;

    if (item) {
      const comments = `${item.descendants} comments`;
      const by = ` by ${item.by}`;
      const time = moment.unix(item.time).fromNow();

      return (
        <Link to={`item/${item.id}`}>
          <div className="StoryRow">
            <span className="StoryRow--index">{this.props.index}</span>
            <span className="StoryRow--right">
              <h1 className="title is-5">{item.title}</h1>
              <div className="StoryRow--info">
                <h2 className="subtitle is-6">{comments}</h2>
                <h2 className="subtitle is-6">{by}</h2>
                <h2 className="subtitle is-6">{time}</h2>
              </div>
            </span>
          </div>
        </Link>
      );
    } else {
      return null;
    }
  }
}

StoryRow.propTypes = {
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired
}

export default StoryRow;